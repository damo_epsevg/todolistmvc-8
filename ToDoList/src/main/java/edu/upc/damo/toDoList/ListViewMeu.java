package edu.upc.damo.toDoList;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListAdapter;
import android.widget.ListView;

/**
 * Created by Josep M on 04/11/2015.
 */
public class ListViewMeu extends ListView {

    // Constructiors exigits pel framework

    public ListViewMeu(Context context, AttributeSet attribute, int defStyleAtt, int defStyleRes) {
        super(context, attribute, defStyleAtt, defStyleRes);
    }

    public ListViewMeu(Context context, AttributeSet attribute, int defStyleAtt) {
        super(context, attribute, defStyleAtt);
    }

    public ListViewMeu(Context context, AttributeSet attribute) {
        super(context, attribute);
    }


    // Modifiquem setAdapter per tal que enregistri la vista com a observadora del model
    // (La crida al super enregistra també la vista com a obseervadora de l'adapter)

    @Override
    public void setAdapter(ListAdapter adapter) {
        super.setAdapter(adapter);
        ModelObservable model = ((AdapterMeu) adapter).getModel();
        model.setOnCanviModelListener(new ModelObservable.OnCanviModelListener() {
            @Override
            public void onNovesDades() {
                invalidateViews();
            }
        });
    }

}
